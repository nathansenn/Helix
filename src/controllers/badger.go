package controllers

import "fmt"
import (
	"github.com/dgraph-io/badger"
)

func (system *P2PSystem) badgerRead(key string) (data []byte){
	item := system.badgerView(key)
	value := badgerValue(item)
	return value
}

func (system *P2PSystem) badgerView(key string) (ritem *badger.Item) {
	fmt.Println("bdb view")
	db := system.badgerDB
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(key))
		if key == "index"{
			if err != nil {
				item = system.creatIndexRange()
			}
		} else {
			handle(err)
		}
		ritem = item
		return nil
	})
	handle(err)
	return ritem
}

func badgerValue(item *badger.Item) (value []byte) {
	fmt.Println("Bdb Value")
	err := item.Value(func(val []byte) error {
		// This func with val would only be called if item.Value encounters no error.

		// Copying or parsing val is valid.
		value = append([]byte{}, val...)
		return nil
	})
	handle(err)
	return value
}

func (system *P2PSystem) badgerSet(hash string, data []byte) {
	db := system.badgerDB
	err := db.Update(func(txn *badger.Txn) error {
		// Start a writable transaction.
		txn = db.NewTransaction(true)
		defer txn.Discard()
		// fmt.Println(data)
		fmt.Println("key ", hash, "--")
		err := txn.Set([]byte(hash), []byte(data))
		txn.Commit()
		return err
	})
	fmt.Println("Data Commited")
	handle(err)
}