package controllers

import (
	"encoding/json"
	"net/http"
	"bytes"
	"fmt"
)

// HTTP CLIENT : SENDING TO OTHER PEERS

func (system *P2PSystem) sendJoin(peer Peer) {
	URL := "http://" + peer.Address + "/join"
	qs, _ := json.Marshal(system.Self)
	resp, err := http.Post(URL, "application/json", bytes.NewBuffer(qs))
	if err != nil {
		system.peerLeft <- peer
		return
	}
	system.PeerJoins <- peer
	defer resp.Body.Close()
	otherPeers := Peers{}
	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&otherPeers)
}

func (system *P2PSystem) sendPkg(peer Peer, msg HelixPkg) {

	//fmt.Println("send to", URL)
	fmt.Println("test")

	err := peer.W.WriteJSON(msg)

	if err != nil {

		fmt.Println("err ", err)
		peer.W.Close()

		PeersReJoin(system, peer)
		//system.peerLeft <- peer
	}


}

