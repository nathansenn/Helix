package controllers

import (
	sh "github.com/ipfs/go-ipfs-api"
	"fmt"
)

func IpfsAddfile(dir string)(string)  {

 	shell := sh.NewShell("localhost:5001")
 	mhash, err := shell.AddDir(dir)

	if err != nil {
		panic(err) // ends where
	}
	fmt.Println(mhash)
	fmt.Printf("Added IPFS hash: %s\n",  mhash)
	return mhash
}


func IpfsGet(mhash string, hash string) error { //create the pads directory
	//connect to ipfs shell
	s := sh.NewShell("localhost:5001")
	err := s.Get(mhash, "../files/HelixPublicFiles/"+hash)
	if err != nil {
		//fmt.Println(err)
		return err
	}
	return nil
}