package controllers

import (
	"os"
	"sync"
	"github.com/dgraph-io/badger"
	"github.com/gorilla/websocket"
	"go.mongodb.org/mongo-driver/mongo"
)

// Define our message object
type Message struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Message  string `json:"message"`
	Etype 	 []byte
	Hash 	 string
	Count	 int
	Range	 int
}

// local file upload
type File struct {
	Filename    string `json:"email"`
	Sha256 string `json:"username"`
	Ipfs  string `json:"message"`
}

// HelixPkg represents a package sent
type HelixPkg struct {
	Message Message
	From    Peer
	Data 	[]byte
	Hash    string
	Ipfs    string
	PType 	string
}

// HelixPkg represents a package sent
type HelixData struct {
	Message string
	From    Peer
	Data 	[]byte
	Hash    string
	Ipfs    string
	PType 	string
}

// Peer represents a other user and machine
type Peer struct {
	Name    string
	Address string
	Pkey string
	W *websocket.Conn
	Active bool
}

// User can store information about the local client
type User struct {
	Password string
}

// Peers is the map of Peers with a address as key
type Peers map[string]Peer

// P2PSystem contains the complete p2p system
type P2PSystem struct {
	Self            Peer
	Peers           Peers
	User 			User
	badgerDB		*badger.DB
	mongo   		*mongo.Client
	ReceivedMsg     chan (HelixPkg)
	PeerJoins       chan (Peer)
	PeerReJoins       chan (Peer)
	peerLeft        chan (Peer)
	CurrentPeers    chan (Peers)
	GetCurrentPeers chan (bool)
	userMsg         chan (HelixPkg)
	interrupt 		chan (os.Signal)
	done 			chan (struct{})
	mu      		sync.Mutex
}

