package controllers

import (
	"fmt"
	"github.com/dgraph-io/badger"
	"github.com/gorilla/websocket"
	"log"
	"net"
	"net/url"
	"os"
	"time"
)

// DOMAIN MODEL
var clients = make(map[*websocket.Conn]bool) // connected clients

var broadcast = make(chan Message)           // broadcast channel


// NewP2PSystem initializes a new P2PSystem and return a *P2PSystem
func NewP2PSystem(self Peer, db *badger.DB) *P2PSystem {
	system := new(P2PSystem)
	system.Self = self
	system.Peers = make(Peers)
	system.badgerDB = db
	system.PeerJoins = make(chan (Peer))
	system.CurrentPeers = make(chan (Peers))
	system.GetCurrentPeers = make(chan (bool))
	system.userMsg = make(chan (HelixPkg))
	system.ReceivedMsg = make(chan (HelixPkg))
	system.interrupt = make(chan os.Signal, 1)
	return system
}

// HEART: P2P SYSTEM
func Start(system *P2PSystem) {
	//system.connectMongo("192.168.254.112", "27017")
	go system.selectLoop(system.Self)
	go system.PeerMaintain()
	go StartWebListener(system)
	fmt.Printf("# \"%s\" listening on %s \n", system.Self.Name, system.Self.Address)
}

func StartBadger() (db *badger.DB) {
	db, err := badger.Open(badger.DefaultOptions("/tmp/badger"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	return db
}

func PeersJoin(system *P2PSystem, peer string)  {
	u := url.URL{Scheme: "ws", Host: peer, Path: "/chat"}
	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil{
		log.Printf("error: %v", err)
		fmt.Println("Trying to reconnect ")
		time.Sleep(5000 * time.Millisecond)
		PeersJoin(system, peer)
	}

	system.PeerJoins <- Peer{"", peer, "", c, true}
	log.Printf("connecting to %s", u.String())

}

func PeersReJoin(system *P2PSystem, peer Peer)  {

	u := url.URL{Scheme: "ws", Host: peer.Address, Path: "/chat"}
	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil{
		log.Printf("error: %v", err)
		fmt.Println("Trying to reconnect ")
		time.Sleep(5000 * time.Millisecond)
		PeersReJoin(system, peer)
	}
	log.Printf("connected to %s", peer.Address)
	system.Peers[peer.Address] = Peer{"", peer.Address, "", c, true}

}

func (system *P2PSystem) PeerMaintain(){
	for  {
		for _, peer := range system.Peers {
			msg := Message{
				Message:"Ping",
			}
			ping := HelixPkg{
				Message: msg,
				From:    system.Self,
				Data:	 nil,
				Hash:    "",
				Ipfs:    "",
				PType: 	 "ping",
			}
			err := peer.W.WriteJSON(ping)
			if err != nil {
				fmt.Println("err ", err)
				peer.W.Close()
				system.Peers[peer.Address] = Peer{"", peer.Address, "", peer.W, false}
				PeersReJoin(system, peer)

			}
			time.Sleep(10 * time.Second)
		}
	}
}

func (system *P2PSystem) selectLoop(sender Peer) {
	for {
		select {
		case msg := <-broadcast:
			fmt.Printf("Localhost GUI broadcast:%s \n", msg.Message)
			var op []byte
			go system.sendGuiMsg(sender, msg, op, "" +
				"","","")
			// Send it out to every client that is currently connected
			for client := range clients {
				err := client.WriteJSON(msg)
				if err != nil {
					log.Printf("error: %v", err)
					client.Close()
					delete(clients, client)
				}
			}

		case peer := <-system.PeerJoins:
			if !system.knownPeer(peer) {
				fmt.Printf("# Connected to: %s \n", peer.Address)
				system.Peers[peer.Address] = peer
				go system.sendJoin(peer)
			}

		case <-system.GetCurrentPeers:
			system.CurrentPeers <- system.Peers

		case peer := <-system.peerLeft:
			log.Println("peer left")
			delete(system.Peers, peer.Address)
		case HelixPkgR := <-system.ReceivedMsg:
				fmt.Println(HelixPkgR.PType)
			if HelixPkgR.PType == "MongoUp"  {
				fmt.Printf("Message: %s\n", HelixPkgR.Message.Message)
				fmt.Printf("IPFS hash: %s\n", HelixPkgR.Ipfs)
				fmt.Printf("Public Key: %s\n", HelixPkgR.From.Pkey)
				fmt.Printf("Hash: %s\n", HelixPkgR.Hash)
				fmt.Println("pull data")
				if HelixPkgR.Message.Message == "index" {
					system.indexHash(HelixPkgR.Hash)
				}
				system.badgerSet(HelixPkgR.Hash, HelixPkgR.Data)
				msg := Message{
					Message:"Data Committed",
				}
				datapkg := HelixPkg{
					Message: msg ,
					From:    system.Self,
					Data:	 HelixPkgR.Data,
					Hash:    HelixPkgR.Hash,
					Ipfs:    "",
					PType: 	"MongoCommitted",
				}
				//fmt.Println("send commit")
				for _, peer := range system.Peers {
					if peer.Address == HelixPkgR.From.Address{
						go system.sendPkg(peer, datapkg)
					}
				}
			}  else if HelixPkgR.PType == "MongoDown"  {
			fmt.Printf("Message: %s\n", HelixPkgR.Message.Message)
			fmt.Printf("IPFS hash: %s\n", HelixPkgR.Ipfs)
			fmt.Printf("Public Key: %s\n", HelixPkgR.From.Pkey)
			fmt.Printf("Hash: %s\n", HelixPkgR.Hash)

			data := system.badgerRead(HelixPkgR.Hash)
			datapkg := HelixPkg{
				Message: HelixPkgR.Message,
				From:    system.Self,
				Data:	 data,
				Hash:    HelixPkgR.Hash,
				Ipfs:    "",
				PType: 	 "MongoDataR",
			}
				for _, peer := range system.Peers {
					if peer.Address == HelixPkgR.From.Address{
						go system.sendPkg(peer, datapkg)
					}
				}
				
			} else if HelixPkgR.PType == "MongoCommitted" {
				fmt.Printf("Message: %s\n", HelixPkgR.Message.Message)
				fmt.Printf("Hash: %s\n", HelixPkgR.Hash)
			} else if HelixPkgR.PType == "MongoDataR" {
				fmt.Printf("Message: %s\n", HelixPkgR.Message.Message)
				fmt.Printf("Public Key: %s\n", HelixPkgR.From.Pkey)
				fmt.Printf("Hash: %s\n", HelixPkgR.Hash)
				system.datar(HelixPkgR)
			} else if HelixPkgR.PType == "file" {
				fmt.Printf("Message: %s\n",  HelixPkgR.Message)
				fmt.Printf("IPFS hash: %s\n",  HelixPkgR.Ipfs)
				fmt.Printf("Public Key: %s\n",  HelixPkgR.From.Pkey)
				IpfsGet(HelixPkgR.Ipfs, HelixPkgR.Message.Message)
				IpfsAddfile("../files/HelixPublicFiles/"+HelixPkgR.Message.Message)
			}

		case HelixPkgS := <-system.userMsg:
			if HelixPkgS.PType == "MongoUp"  {
				fmt.Printf("Hash: %s\n", HelixPkgS.Hash)
				fmt.Printf("Type: %s\n", HelixPkgS.PType)
			} else if HelixPkgS.PType == "MongoPull"  {
				fmt.Printf("Message: %s\n", HelixPkgS.Message.Message)
				fmt.Printf("IPFS hash: %s\n", HelixPkgS.Ipfs)
				fmt.Printf("Public Key: %s\n", HelixPkgS.From.Pkey)
				fmt.Printf("Hash: %s\n", HelixPkgS.Hash)
				fmt.Printf("Type: %s\n", HelixPkgS.PType)
			} else {
				fmt.Printf("%s Localhost Message broadcast: %s\n", HelixPkgS.From.Name, HelixPkgS.Message)
				fmt.Printf("IPFS hash: %s\n", HelixPkgS.Ipfs)
				fmt.Printf("Public Key: %s\n", HelixPkgS.From.Pkey)
				fmt.Printf("Type: %s\n", HelixPkgS.PType)
			}
			for _, peer := range system.Peers {
				go system.sendPkg(peer, HelixPkgS)
			}

		case <-system.interrupt:
			log.Println("interrupt")
			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			for _, peer := range system.Peers {
				err := peer.W.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
				fmt.Println("websocket disconnect")
				if err != nil {
					log.Println("write close:", err)
				}
			}
			os.Exit(0)
		}
	}
}

func (system *P2PSystem) knownPeer(peer Peer) bool {
	if peer.Address == system.Self.Address {
		return true
	}
	_, knownPeer := system.Peers[peer.Address]
	return knownPeer
}

func GetLocalIpv4() string {
	host, _ := os.Hostname()
	addrs, _ := net.LookupIP(host)
	for _, addr := range addrs {
		if ipv4 := addr.To4(); ipv4 != nil {
				return fmt.Sprintf("%s", ipv4)
			}
		}
	return "localhost"
}