package controllers

import (
	"net/http"
	"log"
	"flag"
	"github.com/gorilla/websocket"
	"os"
	"os/signal"
)

// INITIALIZATION

func Helix() {

	port := flag.String("p", "13690", "Listen on port number")
	name := flag.String("n", "anonymous", "Nickname")
	peer := flag.String("j", "", "Other peer to join")
	privateKey := flag.String("k", "", "Private Key")
	flag.Parse()
	Pkey := PublicKey(*privateKey)

	var pw *websocket.Conn
	db := StartBadger()
	system := NewP2PSystem(Peer{*name,  "0.0.0.0" + ":" + *port, Pkey, pw, true}, db)
	signal.Notify(system.interrupt, os.Interrupt)
	Start(system)
	PeersJoin(system, *peer)
	// Create a simple file server
	fs := http.FileServer(http.Dir("../helix-vue"))
	http.Handle("/", fs)
	http.HandleFunc("/viewindex", system.viewIndex)
	http.HandleFunc("/index", system.index)
	http.HandleFunc("/conMongo", system.connectMongoDB)
	http.HandleFunc("/postPublicImage", system.uploadIndexImage)
	http.HandleFunc("/getbase", system.GetMongoDBs)
	http.HandleFunc("/col", system.GetMongoCOLs)
	http.HandleFunc("/upload", system.UploadFile)
	http.HandleFunc("/download", system.Download)
	http.HandleFunc("/datamap", system.Datamap)
	http.HandleFunc("/upDb", system.UploadMongoDB)
	http.HandleFunc("/downDb", system.DownloadMongoDB)
	// Configure websocket route
	http.HandleFunc("/ws", HandleConnections)

	go func(){
		// Start the server on localhost port 8181 and log any errors
		log.Println("http server started on :8181")
		err := http.ListenAndServe(":8181", nil)
		if err != nil {
			log.Fatal("ListenAndServe: ", err)
		}
	}()

	StartStdinListener(system)

}