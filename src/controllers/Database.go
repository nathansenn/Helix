package controllers

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"github.com/dgraph-io/badger"
	"go.mongodb.org/mongo-driver/bson"
	"io"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strconv"
	"unsafe"
)

type Idkey struct {
	Id string
}

func (system *P2PSystem) pushdata(idhash string, id string, hash string, data bson.Raw, pas string){

	//fmt.Println("Bigdata: ", data)
	//encrypt data
	encryptdata := encrypt(data, pas)

	fmt.Printf("Encrypted: %x\n", encryptdata)

	system.badgerSet(hash, encryptdata)

	var msg Message
	system.sendGuiMsg(system.Self, msg, encryptdata, hash, "none", "MongoUp")

	value := system.badgerRead(idhash)

	var ob DbKey

	////fmt.Println("test", valCopy2)
	_ = json.Unmarshal(value, &ob)
	//fmt.Println("meta ", ob.Meta)
	dbkeys := DbKey{
		Range: ob.Range + 1,
		Title: ob.Title,
		Dis: ob.Dis,
		Image: ob.Image,
		Provider: ob.Provider,
	}.encodeDbKey()

	//fmt.Println("Write data key ", dbkeys)
	json.Marshal(dbkeys)

	system.badgerSet(idhash,dbkeys)
	system.sendGuiMsg(system.Self, msg, dbkeys, idhash, "none", "MongoUp")

	rangec := strconv.Itoa(ob.Range)
	//fmt.Println("hash ", hash)
	//fmt.Println("rangec ", rangec)
	newhash := idhash+rangec
	//fmt.Println("newhash ", newhash)

	idkey := Idkey{
		Id: id,
	}.encodeIdKey()
	//fmt.Println("idkey", idkey)

	json.Marshal(idkey)
	system.badgerSet(newhash, idkey)
	system.sendGuiMsg(system.Self, msg, idkey, newhash, "none", "MongoUp")
}

func pulldata(idhash string, hash string, db *badger.DB, pas string) (decryptedv []byte) {
	var valCopy2 []byte
	_ = db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(hash))
		if err != nil {
			log.Fatal(err)
		}

		var valCopy []byte
		_ = item.Value(func(val []byte) error {
			// This func with val would only be called if item.Value encounters no error.

			// Accessing val here is valid.
			////fmt.Println(string(val))
			// Copying or parsing val is valid.
			valCopy = append([]byte{}, val...)
			//fmt.Println("key  ", valCopy)


			////fmt.Println("test", valCopy2)
			return nil
		})
		valCopy2 = append([]byte{}, valCopy...)
		return nil
	})

	var ob Idkey
	_ = json.Unmarshal(valCopy2, &ob)
	//fmt.Println("key  ", ob.Id)

	rhash := ob.Id + idhash
	fmt.Println("key  ", rhash)
	var valCopy4 []byte
	_ = db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(rhash))
		if err != nil {
			log.Fatal(err)
		}

		var valCopy3 []byte
		_ = item.Value(func(val []byte) error {
			// This func with val would only be called if item.Value encounters no error.

			// Accessing val here is valid.
			////fmt.Println(string(val))
			// Copying or parsing val is valid.
			valCopy3 = append([]byte{}, val...)
			//fmt.Println("key1  ", valCopy3)
			//fmt.Println(valCopy4)
			////fmt.Println("test", valCopy2)
			return nil
		})
		valCopy4 = append([]byte{}, valCopy3...)
		return nil
	})
	var ob1 map[string]interface{}
	////fmt.Println("test", valCopy2)
	_ = bson.Unmarshal(valCopy4, &ob1)
	//fmt.Println(valCopy4)


	plaintext := decrypt(valCopy4, pas)
	//fmt.Println("Bson  ", plaintext)
	_ = bson.Unmarshal(plaintext, &ob1)
	//fmt.Println("Bson  ", ob1)
	return plaintext
}

func pulltest(idhash string, hash string, db *badger.DB, pas string)  {

}



func encrypt(data []byte, passphrase string) []byte {
	block, _ := aes.NewCipher([]byte(createHash(passphrase)))
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)
	return ciphertext
}

func decrypt(data []byte, passphrase string) []byte {
	key := []byte(createHash(passphrase))
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err.Error())
	}
	return plaintext
}

func encryptFile(filename string, data []byte, passphrase string) {
	f, _ := os.Create(filename)
	defer f.Close()
	f.Write(encrypt(data, passphrase))
}

func decryptFile(filename string, passphrase string) []byte {
	data, _ := ioutil.ReadFile(filename)
	return decrypt(data, passphrase)
}

func BytesToString(b []byte) string {
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := reflect.StringHeader{bh.Data, bh.Len}
	return *(*string)(unsafe.Pointer(&sh))
}