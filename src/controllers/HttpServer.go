package controllers

import (
	"net/http"
	"log"
	"encoding/json"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{} // use default options

func StartWebListener(system *P2PSystem) {
	http.HandleFunc("/chat", readPkg(system))
	http.HandleFunc("/join", createJoinHandler(system))
	log.Fatal(http.ListenAndServe(system.Self.Address, nil))
}

func createJoinHandler(system *P2PSystem) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		joiner := Peer{}
		dec := json.NewDecoder(r.Body)
		err := dec.Decode(&joiner)
		if err != nil {
			log.Printf("Error unmarshalling from: %v", err)
		}

		system.GetCurrentPeers <- true
		enc := json.NewEncoder(w)
		enc.Encode(<-system.CurrentPeers)
	}
}

func readPkg(system *P2PSystem) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var pkg HelixPkg
		c, err := upgrader.Upgrade(w, r, nil)
		handle(err)
		defer c.Close()
		for {

			err = c.ReadJSON(&pkg)
			if err != nil {
				log.Println("write:", err)
				break
			}
			//fmt.Println("pkg ", pkg.Hash)
			system.ReceivedMsg <- pkg


		}
	}
}

func createChatHandler(system *P2PSystem) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c, err := upgrader.Upgrade(w, r, nil)
		handle(err)
		mt, message, err := c.ReadMessage()
		cm := HelixPkg{}
		json.Unmarshal(message, cm)
		log.Printf("recv: %s", message)
		err = c.WriteMessage(mt, message)
		handle(err)
		system.ReceivedMsg <- cm
	}
}
