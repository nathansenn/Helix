// using mgo package - https://github.com/globalsign/mgo
// Tests - https://github.com/globalsign/mgo/blob/master/session_test.go
package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgraph-io/badger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"io/ioutil"
	"os"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"strconv"
	"mime/multipart"
)

type DbKey struct {
	Range int
	Title 		string
	Dis 		string
	Image 		string
	Provider 	string
}

type IndexHash struct {
	Hash string
}

type MongoKeyFile struct {
	DatabaseName  string
	CollectionName   string
	CollectionHash   string
	Password   string
}

func (system *P2PSystem) GetMongoDBs(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	// connect to MongoDB
	client := system.mongo
    listdb, err := client.ListDatabaseNames(context.Background(), bsonx.Doc{})

	handle(err)
	var slcD []string
	for _, c := range listdb {
		//fmt.Println(c)
		slcD = append(slcD, c)
	}

	slcB, _ := json.Marshal(slcD)
	//fmt.Println(string(slcB))

	JsonResponse(w, http.StatusCreated, string(slcB))

	//db := client.Database("bigbackup")
	//inventory := db.Collection("info_consumer_base_mongos")

	//cur, err := inventory.Find(context.Background(), nil)
	//if err != nil { log.Fatal(err) }
	//defer cur.Close(context.Background())
	//for cur.Next(context.Background()) {
	//	raw, err := cur.DecodeBytes()
	//	var ob map[string]interface{}
	//	_ = bson.Unmarshal(raw, &ob)
	//	//fmt.Println(ob["_id"].(primitive.ObjectID).Hex())
	//	//fmt.Println("Push data")
	//	pushdata(ob["_id"].(primitive.ObjectID).Hex(), raw)
	//	//fmt.Println("Pull data")
	//	pulldata(ob["_id"].(primitive.ObjectID).Hex())
	//	if err != nil { log.Fatal(err) }
	//	break
		// do something with elem....

	//}
	//if err := cur.Err(); err != nil {
	//	return
	//}

}

func (system *P2PSystem) GetMongoCOLs(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	// connect to MongoDB
	println("1")
	client := system.mongo

	println(r.FormValue("db"))
	db := client.Database(r.FormValue("db"))

	cur, err := db.ListCollections(context.Background(), bsonx.Doc{})
	if err != nil { log.Fatal(err) }
	println("2")
	var slcD []string
	println("2")
	for cur.Next(context.Background()) {
		println("3")
		raw := cur.Current
		println("3")
		var ob map[string]interface{}
		println("3")
		_ = bson.Unmarshal(raw, &ob)
		println("3")
		//fmt.Println(ob["name"])
		println("3")
		c := ob["name"].(string)
		println("3")
		slcD = append(slcD, c)
	}
	defer cur.Close(context.Background())

	println("4")

	slcB, _ := json.Marshal(slcD)
	//fmt.Println(string(slcB))

	JsonResponse(w, http.StatusCreated, string(slcB))



}

func (system *P2PSystem) genMogoDbKey(colname string, dbname string, pas string) (string) {
	dbkeyk, _:= GenerateRandomString(64)
	//fmt.Println("Gen Key")
	//fmt.Println(dbkeyk)
	keyfile := MongoKeyFile{
		DatabaseName: dbname,
		CollectionName: colname,
		CollectionHash: dbkeyk,
		Password: pas,
	}
	rankingsJson, _ := json.Marshal(keyfile)
	ioutil.WriteFile("../keys/mongo/"+dbname+"-"+colname+"-"+dbkeyk+".key", rankingsJson, 0777)
	//fmt.Printf("%+v", keyfile)
	return dbkeyk

}

func (system *P2PSystem) connectMongoDB(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	ip := r.FormValue("ip")
	port := r.FormValue("port")
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://"+ip+":"+port), nil)
	if err != nil {
		log.Fatal(err)
	}
	system.mongo = client
	JsonResponse(w, http.StatusCreated, string("Success"))
}

func (system *P2PSystem) connectMongo(ip string, port string) {

	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://"+ip+":"+port), nil)
	if err != nil {
		log.Fatal(err)
	}
	system.mongo = client
}

func conBadgerDB() (bdb *badger.DB, err error) {
	db, err := badger.Open(badger.DefaultOptions("/tmp/badger"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	return db, err
}

func (system *P2PSystem) uploadIndexImage(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	file, handle, err := r.FormFile("file")
	if err != nil {
		fmt.Println(r.FormFile("file"))
		fmt.Fprintf(w, "%v", err)
		return
	}
	hash := addImage(file, handle)

	JsonResponse(w, http.StatusCreated, string(hash))
}

func (system *P2PSystem) UploadMongoDB(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	colname := r.FormValue("col")
	dbname := r.FormValue("db")
	pas := r.FormValue("pas")
	title := r.FormValue("title")
	dis := r.FormValue("dis")
	provider := r.FormValue("provider")
	image := r.FormValue("image")

	// gen DB key
	dbkeyk := system.genMogoDbKey(colname, dbname, pas)

	// push Database key
	system.pushDBKey(dbkeyk, title, dis, provider, image)

	// connect to MongoDB
	client := system.mongo

	db := client.Database(r.FormValue("db"))
	inventory := db.Collection(r.FormValue("col"))

	var slcD []string
	cur, err := inventory.Find(context.Background(), bsonx.Doc{})
	if err != nil { log.Fatal(err) }
	i := 0
	for cur.Next(context.Background()) {
		raw := cur.Current
		var ob map[string]interface{}
		_ = bson.Unmarshal(raw, &ob)
		id := ob["_id"].(primitive.ObjectID).Hex()
		id2 := id+dbkeyk
		//fmt.Println(id2)
		//fmt.Println("Push data")
		system.pushdata(dbkeyk, id, id2, raw, pas)
		slcD = append(slcD, id2)
		i++
		if i == 100000{
			break
		}

	}
	cur.Close(context.Background())
	slcB, _ := json.Marshal(slcD)
	//fmt.Println(string(slcB))
	JsonResponse(w, http.StatusCreated, string(slcB))
}

func (system *P2PSystem) pushDBKey(dbKeyk string, title string, dis string, provider string, image string)  {

	dbkeys := DbKey{
		Range: 0,
		Title: title,
		Dis: dis,
		Image: image,
		Provider: provider,
	}.encodeDbKey()

	json.Marshal(dbkeys)
	system.badgerSet(dbKeyk, dbkeys)

	var msg Message
	msg.Message = "index"
	system.sendGuiMsg(system.Self, msg, dbkeys, dbKeyk, "none", "MongoUp")
}

func (system *P2PSystem) DownloadMongoDB(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	file, handle, err := r.FormFile("file")
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}
	err = ioutil.WriteFile("../keys/imported/mongo/"+handle.Filename, data, 0777)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}

	jsonFile, err := os.Open("../keys/imported/mongo/"+handle.Filename)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = jsonFile.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var keyFile MongoKeyFile
	json.Unmarshal(byteValue, &keyFile)
	//fmt.Println("Database Name: " + keyFile.DatabaseName)
	//fmt.Println("Collection Name: " + keyFile.CollectionName)
	//fmt.Println("Collection Hash: " + keyFile.CollectionHash)
	//fmt.Println("Password: " + keyFile.Password)
	system.User.Password = keyFile.Password
	tmsg := []byte("Monogocol")

	emsg := encrypt(tmsg, keyFile.Password)
	msg := Message{
		Etype: emsg,
	}
	var dataf []byte

	system.sendGuiMsg(system.Self, msg, dataf, keyFile.CollectionHash, "none", "MongoDown")


}

func (system *P2PSystem) datar(pkg HelixPkg)  {
	data := pkg.Data
	hash := pkg.Hash
	pass := system.User.Password
	mtype := decrypt(pkg.Message.Etype, pass)

	t := string(mtype[:])
	fmt.Println("t ", t)
	if t == "Monogocol" {
		var ob DbKey
		_ = json.Unmarshal(pkg.Data, &ob)
		fmt.Println("key  ", ob.Range)

		for i := 0; i < ob.Range - 1; i++ {
			id := strconv.Itoa(i)
			id2 := pkg.Hash+id
			fmt.Println("Pull data")
			fmt.Println("sum  ", i)
			var dataf []byte
			tmsg := []byte("MonogoId")
			emsg := encrypt(tmsg, pass)
			msg := Message{
				Etype: emsg,
				Hash: hash,
			}
			//fmt.Println("breaks")
			datapkg := HelixPkg{
				Message: msg ,
				From:    system.Self,
				Data:	 dataf,
				Hash:    id2,
				Ipfs:    "",
				PType: 	"MongoDown",
			}
			for _, peer := range system.Peers {
				go system.sendPkg(peer, datapkg)
			}

		}
	}
	if t == "MonogoId" {
		var ob Idkey
		_ = json.Unmarshal(data, &ob)
		fmt.Println("key  ", ob.Id)
		rhash := ob.Id + pkg.Message.Hash
		fmt.Println("key  ", rhash)
		var dataf []byte
		tmsg := []byte("MongoData")
		emsg := encrypt(tmsg, pass)
		msg := Message{
			Etype: emsg,
			Hash: pkg.Message.Hash,
		}

		datapkg := HelixPkg{
			Message: msg ,
			From:    system.Self,
			Data:	 dataf,
			Hash:    rhash,
			Ipfs:    "",
			PType: 	"MongoDown",
		}

		for _, peer := range system.Peers {
			go system.sendPkg(peer, datapkg)
		}

	}
	if t == "MongoData" {
		fmt.Println("final data")
		var ob1 map[string]interface{}
		////fmt.Println("test", valCopy2)
		_ = bson.Unmarshal(data, &ob1)

		plaintext := decrypt(data, pass)

		_ = bson.Unmarshal(plaintext, &ob1)
		fmt.Println("Bson  ", ob1)
	}
}

func getIdKey(bdb *badger.DB)  {
	_ = bdb.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte("1"))
		if err != nil {
			log.Fatal(err)
		}

		var valCopy []byte
		_ = item.Value(func(val []byte) error {
			// This func with val would only be called if item.Value encounters no error.

			// Accessing val here is valid.
			////fmt.Println(string(val))
			// Copying or parsing val is valid.
			valCopy = append([]byte{}, val...)
			//fmt.Println("key  ", valCopy)
			var ob DbKey

			////fmt.Println("test", valCopy2)
			_ = json.Unmarshal(valCopy, &ob)
			//fmt.Println("key  ", ob)
			return nil
		})
		return nil
	})
}

func (k DbKey) encodeDbKey() []byte  {
	data, err := json.Marshal(k)
	if err != nil {
		panic(err)
	}
	return data
}

func (k Idkey) encodeIdKey() []byte  {
	data, err := json.Marshal(k)
	if err != nil {
		panic(err)
	}
	return data
}

func addImage(file multipart.File, handle *multipart.FileHeader)(string)  {
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}

	err = ioutil.WriteFile("../files/"+handle.Filename, data, 0777)
	if err != nil {
		fmt.Println(err)
	}

	f, err := os.Open("../files/"+handle.Filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	hash := IpfsAddfile("../files/"+handle.Filename)
	return hash
}

func (k indexRange) encodeIRange() []byte  {
	data, err := json.Marshal(k)
	if err != nil {
		panic(err)
	}
	return data
}

func (k IndexHash) encodeIndexhash() []byte  {
	data, err := json.Marshal(k)
	if err != nil {
		panic(err)
	}
	return data
}