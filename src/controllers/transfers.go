package controllers

import (
	"bufio"
	"os"
	"log"
	"fmt"
)

// LISTENER STANDARD IN : USER INPUT
func StartStdinListener(system *P2PSystem) {
	reader := bufio.NewReader(os.Stdin)

	for {
		line, _ := reader.ReadString('\n')
		message := line[:len(line)-1]
		var op []byte
		msg := Message{
			Message:message,
		}
		system.userMsg <- HelixPkg{msg, system.Self, op, "" ,"","message"}
	}

}

func (system *P2PSystem) sendGuiMsg(sender Peer, msg Message, data []byte, hash string, ipfs string, ptype string) {
	fmt.Println("msg")
	system.userMsg <- HelixPkg{msg, sender, data, hash ,ipfs,ptype}
}

func (system *P2PSystem) sendFile(sender Peer, file string, hash string) {
	var op []byte
	msg := Message{
		Message:hash,
	}
	system.userMsg <- HelixPkg{msg, sender, op, "", file, "file"}
	log.Println(file)
}

func BroadcastFiles(system *P2PSystem, file string, hash string){
	go system.sendFile(system.Self ,file, hash)

}


