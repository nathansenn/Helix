package controllers

import (
	"encoding/json"
	"strconv"
	"github.com/dgraph-io/badger"
	"fmt"
	"net/http"
)

type indexRange struct {
	iRange int
}

func (system *P2PSystem) viewIndex(w http.ResponseWriter, r *http.Request)  {
	enableCors(&w)
	iRange := system.getIndexRange()
	var ob DbKey

	////fmt.Println("test", valCopy2)
	_ = json.Unmarshal(iRange, &ob)
	fmt.Println("meta ", ob.Range)
	inRange := ob.Range

	fmt.Println("key  ", iRange)
	for i := 0; i < inRange; i++ {
		si := strconv.Itoa(i)
		is := "index"+si
		data := system.badgerRead(is)
		var ob1 IndexHash
		////fmt.Println("test", valCopy2)
		_ = json.Unmarshal(data, &ob1)
		fmt.Println("hash ", ob1.Hash)
		meta := system.badgerRead(ob1.Hash)
		var ob2 DbKey
		_ = json.Unmarshal(meta, &ob2)
		fmt.Println("meta Title ", ob2.Title)
		JsonResponse(w, http.StatusCreated, string(meta))
	}
}

func (system *P2PSystem) indexHash(hash string) {
	fmt.Println("index hash")

	value := system.badgerRead("index")

	var ob DbKey

	////fmt.Println("test", valCopy2)
	_ = json.Unmarshal(value, &ob)
	fmt.Println("meta ", ob.Range)
	dbkeys := DbKey{
		Range: ob.Range + 1,
	}.encodeDbKey()

	//fmt.Println("Write data key ", dbkeys)
	json.Marshal(dbkeys)
	system.badgerSet("index", dbkeys)

	s := strconv.Itoa(ob.Range)
	ihash := IndexHash{
		Hash: hash,
	}.encodeIndexhash()

	b := []byte(ihash)
	key := "index"+s
	system.badgerSet(key, b)

}

func (system *P2PSystem) getIndexRange() ([]byte){
	iRange := system.badgerRead("index")
	return iRange
}

func (system *P2PSystem) checkIndex()  {
	system.badgerRead("index")
}

func (system *P2PSystem) index(w http.ResponseWriter, r *http.Request) {
	fmt.Println("index")
	enableCors(&w)
	title := r.FormValue("title")
	dis := r.FormValue("dis")
	provider := r.FormValue("provider")
	image := r.FormValue("image")
	dbKeyk := genIndexKey()
	dbkeys := DbKey{
		Range: 0,
		Title: title,
		Dis: dis,
		Image: image,
		Provider: provider,
	}.encodeDbKey()

	json.Marshal(dbkeys)
	system.badgerSet(dbKeyk, dbkeys)
	system.indexHash(dbKeyk)
	var msg Message
	msg.Message = "index"
	system.sendGuiMsg(system.Self, msg, dbkeys, dbKeyk, "none", "MongoUp")
}

func (system *P2PSystem) creatIndexRange() (item *badger.Item) {
	fmt.Println("making index")
	dbkeys := DbKey{
		Range: 0,
	}.encodeDbKey()

	json.Marshal(dbkeys)
	system.badgerSet("index", dbkeys)
	item = system.badgerView("index")
	return item
}