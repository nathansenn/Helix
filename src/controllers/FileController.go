package controllers

import (
	"os/exec"
	"fmt"
	"net/http"
	"mime/multipart"
	"io/ioutil"
	"os"
	"log"
	"io"
	"encoding/hex"
	"crypto/sha256"
	"encoding/json"
	"bufio"
	"runtime"
)

func (system *P2PSystem) Download(w http.ResponseWriter, r *http.Request)  {
	enableCors(&w)
	fmt.Println("download")
	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	file, handle, err := r.FormFile("file")
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(file)
		fmt.Fprintf(w, "%v", err)
		return
	}

	err = ioutil.WriteFile("../files/buildfiles/"+handle.Filename, data, 0777)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}

	fn, err := os.Open("../files/buildfiles/"+handle.Filename)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = fn.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	var a string
	var b string
	var a1 string
	var b1 string
	var a2 string
	var b2 string
	var bh string
	var bn string
	sc := bufio.NewScanner(fn)
	lastLine := 0
	for sc.Scan() {
		lastLine++
		if lastLine == 2 {
			bn = sc.Text()
			fmt.Println(bn)
		}
		if lastLine == 4 {
			bh = sc.Text()
			fmt.Println(bh)
		}
		if lastLine == 6 {
			a = sc.Text()
			fmt.Println(a)
		}
		if lastLine == 8 {
			b = sc.Text()
			fmt.Println(b)
		}
		if lastLine == 10 {
			a1 = sc.Text()
			fmt.Println(a1)
		}
		if lastLine == 12 {
			b1 = sc.Text()
			fmt.Println(b1)
		}
		if lastLine == 14 {
			a2 = sc.Text()
			fmt.Println(a2)
		}
		if lastLine == 16 {
			b2 = sc.Text()
			fmt.Println(b2)
		}
	}



	fmt.Println(bh)
	if bh != "0"{
		fmt.Println(bh)
		os.MkdirAll("../files/shards/"+bh+"/", os.ModePerm)
		if a != "0" && b != "0"{
			IpfsGet(b, a)
			input, err := ioutil.ReadFile("../files/HelixPublicFiles/"+a)
			if err != nil {
				fmt.Println(err)
				return
			}
			err = ioutil.WriteFile("../files/shards/"+bh+"/"+a, input, 0777)
			if err != nil {
				fmt.Println("Error creating", "../files/shards/"+bh+"/"+a)
				fmt.Println(err)
				return
			}
		}
		if a1 != "0" && b1 != "0"{
			IpfsGet(b1, a1)
			input, err := ioutil.ReadFile("../files/HelixPublicFiles/"+a1)
			if err != nil {
				fmt.Println(err)
				return
			}
			err = ioutil.WriteFile("../files/shards/"+bh+"/"+a1, input, 0777)
			if err != nil {
				fmt.Println("Error creating", "../files/shards/"+bh+"/"+a1)
				fmt.Println(err)
				return
			}
		}
		if a2 != "0" && b2 != "0"{
			IpfsGet(b2, a2)
			input, err := ioutil.ReadFile("../files/HelixPublicFiles/"+a2)
			if err != nil {
				fmt.Println(err)
				return
			}
			err = ioutil.WriteFile("../files/shards/"+bh+"/"+a2, input, 0777)
			if err != nil {
				fmt.Println("Error creating", "../files/shards/"+bh+"/"+a2)
				fmt.Println(err)
				return
			}
		}
	}

	slcD := []string{bh, bn}
	slcB, _ := json.Marshal(slcD)
	fmt.Println(string(slcB))


	sh, err2 := os.Create("../self_encryption/file_decrypt.sh")
	if err2 != nil {
		log.Fatal("Cannot create file", err2)
	}
	defer file.Close()

	fmt.Fprintf(sh, "cargo run --example basic_encryptor -- -d  ../files/"+bn+" "+bh+ " ../files/keys/"+bh+"/data_map")
	sh.Close()
	if runtime.GOOS == "windows" {
		cmd:= exec.Command("cargo", "run", "--example", "basic_encryptor", "--", "-d", "../files/"+bn, bh, "../files/keys/"+bh+"/data_map")
		cmd.Dir = "../self_encryption"
		out, err := cmd.Output()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(out))

	} else{
		cmd:= exec.Command("sh", "file_decrypt.sh")
		cmd.Dir = "../self_encryption"
		out, err := cmd.Output()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(out))
	}

	JsonResponse(w, http.StatusCreated, string(slcB))

}

func (system *P2PSystem) Datamap(w http.ResponseWriter, r *http.Request)  {
	enableCors(&w)
	// logic part of log in
	fmt.Println("hash "+r.FormValue("hash"))

	fmt.Println("filename "+r.FormValue("filename"))

	file, handle, err := r.FormFile("file")
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}

	err = ioutil.WriteFile("../files/shards/"+r.FormValue("hash")+"/"+handle.Filename, data, 0777)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}
	sh, err2 := os.Create("../self_encryption/file_decrypt.sh")
	if err2 != nil {
		log.Fatal("Cannot create file", err2)
	}
	defer file.Close()

	fmt.Fprintf(sh, "cargo run --example basic_encryptor -- -d ../files/shards/"+r.FormValue("hash")+"/"+handle.Filename+" "+r.FormValue("hash"))

	cmd:= exec.Command("sh", "file_decrypt.sh")
	cmd.Dir = "../self_encryption"
	out, err := cmd.Output()
	fmt.Println(string(out))


	input, err := ioutil.ReadFile("../self_encryption/"+r.FormValue("hash"))
	if err != nil {
		//fmt.Println(err)
		return
	}

	err = ioutil.WriteFile("../files/"+r.FormValue("filename"), input, 0777)
	if err != nil {
		//fmt.Println("Error creating", "../files/"+r.FormValue("filename"))
		//fmt.Println(err)
		return
	}

	http.HandleFunc("/"+handle.Filename, func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "../files/shards/"+r.FormValue("hash")+"/"+handle.Filename)
	})
}

func (system *P2PSystem) UploadFile(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	file, handle, err := r.FormFile("file")
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}
	defer file.Close()
	system.saveFile(w, file, handle)

}

func (system *P2PSystem) saveFile(w http.ResponseWriter, file multipart.File, handle *multipart.FileHeader) {
	enableCors(&w)
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}

	err = ioutil.WriteFile("../files/"+handle.Filename, data, 0777)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}

	f, err := os.Open("../files/"+handle.Filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}
	md := h.Sum(nil)
	mdStr := hex.EncodeToString(md)
	fmt.Printf("%x  \n", h.Sum(nil))

	sh, err2 := os.Create("../self_encryption/file_encrypt.sh")
	if err2 != nil {
		log.Fatal("Cannot create file", err2)
	}
	defer file.Close()

	fmt.Fprintf(sh, "cargo run --example basic_encryptor -- -e ../files/"+handle.Filename+" "+mdStr)
	sh.Close()
	if runtime.GOOS == "windows" {
		cmd:= exec.Command("cargo", "run", "--example", "basic_encryptor", "--", "-e", "../files/"+handle.Filename, mdStr)
		cmd.Dir = "../self_encryption"
		out, err := cmd.Output()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(out))
		
	} else{
		cmd:= exec.Command("sh", "file_encrypt.sh")
		cmd.Dir = "../self_encryption"
		out, err := cmd.Output()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(out))
	}


	bf, err2 := os.Create("../files/buildfiles/"+handle.Filename+".build")
	if err2 != nil {
		log.Fatal("Cannot create file", err2)
	}
	defer file.Close()

	files, err := ioutil.ReadDir("../files/shards/"+mdStr)
	if err != nil {
		log.Fatal(err)
	}

	io.WriteString(bf, "File Name \r\n")
	io.WriteString(bf, handle.Filename +"\r\n")

	io.WriteString(bf, "File hash \r\n")
	io.WriteString(bf, mdStr+"\r\n")

	for _, file := range files {
		io.WriteString(bf, "File Shard Hash \r\n")
		io.WriteString(bf, file.Name()+"\r\n")
		fmt.Println(file.Name())
		io.WriteString(bf, "Ipfs Hash \r\n")
		ipfshash:= IpfsAddfile("../files/shards/"+mdStr+"/"+file.Name())
		io.WriteString(bf, ipfshash+"\r\n")
		BroadcastFiles(system, ipfshash, file.Name())
	}

	JsonResponse(w, http.StatusCreated, "File uploaded successfully!.")
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}