sudo apt-get update
sudo apt-get -y upgrade
curl -O https://storage.googleapis.com/golang/go1.13.linux-amd64.tar.gz
tar -xvf go1.11.2.linux-amd64.tar.gz
sudo mv go /usr/local
sudo nano ~/.profile
export GOPATH=$HOME/work
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
source ~/.profile
export GOPATH=$HOME/work/src
go version
apt-get install build-essential