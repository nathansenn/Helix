# Helix

- Copyright (c) 2017-2018, Helix Project
- This may contain code Copyright (c) IPFS Project

## Development Resources

- Car6on
- Web: [h3lix.io](http://h3lix.io)
- BCT: [BitCoinTalk](https://bitcointalk.org/)

## Build

| Operating System      | Processor | Status |
| --------------------- | -------- |--------|
| Ubuntu 16.04          |  i686    | [![Ubuntu 16.04 i686](https://build.getmonero.org/png?builder=Monero-static-ubuntu-i686)](https://build.getmonero.org/builders/Monero-static-ubuntu-i686)
| Ubuntu 16.04          |  amd64   | [![Ubuntu 16.04 amd64](https://build.getmonero.org/png?builder=Monero-static-ubuntu-amd64)](https://build.getmonero.org/builders/Monero-static-ubuntu-amd64)
| Ubuntu 16.04          |  armv7   | [![Ubuntu 16.04 armv7](https://build.getmonero.org/png?builder=Monero-static-ubuntu-arm7)](https://build.getmonero.org/builders/Monero-static-ubuntu-arm7)
| Debian Stable         |  armv8   | [![Debian armv8](https://build.getmonero.org/png?builder=Monero-static-debian-armv8)](https://build.getmonero.org/builders/Monero-static-debian-armv8)
| OSX 10.10             |  amd64   | [![OSX 10.10 amd64](https://build.getmonero.org/png?builder=Monero-static-osx-10.10)](https://build.getmonero.org/builders/Monero-static-osx-10.10)
| OSX 10.11             |  amd64   | [![OSX 10.11 amd64](https://build.getmonero.org/png?builder=Monero-static-osx-10.11)](https://build.getmonero.org/builders/Monero-static-osx-10.11)
| OSX 10.12             |  amd64   | [![OSX 10.12 amd64](https://build.getmonero.org/png?builder=Monero-static-osx-10.12)](https://build.getmonero.org/builders/Monero-static-osx-10.12)
| FreeBSD 11            |  amd64   | [![FreeBSD 11 amd64](https://build.getmonero.org/png?builder=Monero-static-freebsd64)](https://build.getmonero.org/builders/Monero-static-freebsd64)
| DragonFly BSD 4.6     |  amd64   | [![DragonFly BSD amd64](https://build.getmonero.org/png?builder=Monero-static-dragonflybsd-amd64)](https://build.getmonero.org/Monero/Superior-static-dragonflybsd-amd64)
| Windows (MSYS2/MinGW) |  i686    | [![Windows (MSYS2/MinGW) i686](https://build.getmonero.org/png?builder=Monero-static-win32)](https://build.getmonero.org/builders/Monero-static-win32)
| Windows (MSYS2/MinGW) |  amd64   | [![Windows (MSYS2/MinGW) amd64](https://build.getmonero.org/png?builder=Monero-static-win64)](https://build.getmonero.org/builders/Monero-static-win64)

## Coverage

| Type      | Status |
|-----------|--------|
| Coverity  | [![Coverity Status](https://scan.coverity.com/projects/9657/badge.svg)](https://scan.coverity.com/projects/9657/)
| Coveralls | [![Coveralls Status](https://coveralls.io/repos/github/TheSuperioCoin/TheSuperiorCoin/badge.svg?branch=master)](https://coveralls.io/github/Superior-project/Superior?branch=master)
| License   | [![License](https://img.shields.io/badge/license-BSD3-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

## Introduction
Helix is a decentralised p2p network allowing the transfer and storage of files.


## About this Project

This is the core implementation of Helix. It is open source and completely free to use without restrictions, except for those specified in the license agreement below. There are no restrictions on anyone creating an alternative implementation of Superior that uses the protocol and network in a compatible manner.

As with many development projects, the repository on Github is considered to be the "staging" area for the latest changes. Before changes are merged into that branch on the main repository, they are tested by individual developers in their own branches, submitted as a pull request, and then subsequently tested by contributors who focus on testing and code reviews. That having been said, the repository should be carefully considered before using it in a production environment, unless there is a patch in the repository for a particular show-stopping issue you are experiencing. It is generally a better idea to use a tagged release for stability.

**Anyone is welcome to contribute to Superior's codebase!** If you have a fix or code change, feel free to submit is as a pull request directly to the "master" branch. In cases where the change is relatively small or does not affect other parts of the codebase it may be merged in immediately by any one of the collaborators. On the other hand, if the change is particularly large or complex, it is expected that it will be discussed at length either well in advance of the pull request being submitted, or even directly on the pull request.

## Supporting the Project


## License

See [LICENSE](LICENSE).

# Contributing

If you want to help out, see [CONTRIBUTING](CONTRIBUTING.md) for a set of guidelines.

## Vulnerability Response Process

See [Vulnerability Response Process](VULNERABILITY_RESPONSE_PROCESS.md).

## Helix software updates and consensus protocol changes (hard fork schedule)


| Fork Date              | Consensus version | Minimum Superior Version | Recommended Superior Version | Details            |  
| ----------------- | ----------------- | ---------------------- | -------------------------- | ------------------ |
|        |        |                   |                     |  |
                    

X's indicate that these details have not been determined as of commit date.

## Release staging schedule and protocol

Approximately three months prior to a scheduled mandatory software upgrade, a branch from Master will be created with the new release version tag. Pull requests that address bugs should then be made to both Master and the new release branch. Pull requests that require extensive review and testing (generally, optimizations and new features) should *not* be made to the release branch. 

## Installing TheSuperiorCoin from a package

Packages are available for

* Arch Linux 
  - Stable release: 
  - Bleeding edge: 

* OS X via [Homebrew](http://brew.sh) not ready yet

        

* Docker

        
Packaging for your favorite distribution would be a welcome contribution!

## Compiling Helix from Source

### Dependencies

The following table summarizes the tools and libraries required to build. A
few of the libraries are also included in this repository (marked as
"Vendored"). By default, the build uses the library installed on the system,
and ignores the vendored sources. However, if no library is found installed on
the system, then the vendored source will be built and used. The vendored
sources are also used for statically-linked builds because distribution
packages often include only shared library binaries (`.so`) but not static
library archives (`.a`).

| Dep          | Min. version  | Vendored | Debian/Ubuntu pkg  | Arch pkg     | Fedora            | Optional | Purpose        |
| ------------ | ------------- | -------- | ------------------ | ------------ | ----------------- | -------- | -------------- |
| Golang       |               |          | ``                 | ``           | ``                | NO       |                |
| IPFS         |               |          |                    |              |                   | NO       |                |
| Rust         |               |          |                    |              |                   | NO       |                |
| Cargo        |               |          |                    |              |                   | NO       |                |
      


### Cloning the Repository

Clone recursively to pull-in needed submodule(s):

    git clone --recursive https://github.com/

If you already have a repo cloned, initialize and update:

    cd Helix 
    git submodule init && git submodule update --remote
    
To run Helix:

    cd src 
    go run main.go -n name -p port -j IP:13690 -k key
    p Listen on port number default 13690
    n Nickname default anonymous
    j Other peer to join
    k Private Key
    

### Build instructions



#### On Linux and OS X

* Install the dependencies
* https://medium.com/@patdhlk/how-to-install-go-1-9-1-on-ubuntu-16-04-ee64c073cd79
* curl https://sh.rustup.rs -sSf | sh
* git clone https://github.com/ipfs/go-ipfs

#### On the Raspberry Pi

Tested on a Raspberry Pi Zero with a clean install of minimal Raspbian Stretch (2017-09-07 or later) from https://www.raspberrypi.org/downloads/raspbian/. If you are using Raspian Jessie, [please see note in the following section](#note-for-raspbian-jessie-users). 

* `apt-get update && apt-get upgrade` to install all of the latest software

* Install the dependencies for SuperiorCoin from the 'Debian' column in the table above.

* Increase the system swap size:

#### On Windows:


### On FreeBSD:

### On OpenBSD:

#### OpenBSD < 6.2


#### OpenBSD >= 6.2

### On Solaris:

### On Linux for Android (using docker):

  
### Building Portable Statically Linked Binaries

## Running superiord


## Internationalization

## Using Tor



### Using Tor on Tails


## Debugging


### Obtaining stack traces and core dumps on Unix systems



### Analysing memory corruption



### LMDB

